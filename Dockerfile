FROM ubuntu

WORKDIR /

RUN  apt-get update \
  && apt-get install -y wget

COPY certificate.pem .
COPY key.pem .
COPY OoklaServer-linux64.tgz .
COPY OoklaServer.properties .
COPY install_server.sh .

RUN cp OoklaServer.properties OoklaServer.properties.default
RUN chmod a+x install_server.sh
RUN ./install_server.sh install -f

EXPOSE 8080
EXPOSE 5060


CMD ["./OoklaServer"]
